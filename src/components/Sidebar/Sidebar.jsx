import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './Sidebar.scss';


class Sidebar extends Component {

    static propTypes = {
        items: PropTypes.array.isRequired
    };

    render() {

        const { togglestate, items } = this.props;

        return (
            <div className={`app-sidebar sidenava ${togglestate ? '' : 'sidenav'}`}>
                <div className="side-menu">
                    <ul className="lu-side-menu">
                        {items && items.map((item, key) => <li key={key} className={`li-side-menu  ${item.url === this.props.path ? "active-l":""} `}> <Link to={item.url} className={item.classNam}><i className={item.icon}></i><span></span>{item.name}</Link></li>)}
                    </ul>
                </div>
            </div>
        );
    }
}

export default Sidebar;