import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import './DefaultLayout.scss';

import nav from '../../Util/Nav';
import Header from '../../components/Header/Header';
import Sidebar from '../../components/Sidebar/Sidebar';

import Config  from '../../components/Config/Config';
import Home  from '../../components/Home/Home';
import Others from '../../components/Others/Others';
import Products from '../../components/Products/Products'
import Users from '../../components/Users/Users'
import Dashboard from '../../components/Dashboard/Dashboard'


import ReactTimeout from 'react-timeout';

class DefaultLayout extends Component {

    state = {
        ButtonToggle: true,
        isActivelogin: null,
        opacity: false,
        opacityNone: false
      }
    
    ChangeToggle = () => {
        this.setState((prevState,props) => {
          return {
              ButtonToggle: !prevState.ButtonToggle
            }
        });
    }

    componentWillMount= () => {
        this.changeOpacity();
    };

    opacity = () => {
        this.setState({
            opacity: true
        })
    }

    opacityNone=() =>{
        this.setState({
            opacityNone: true
        })
    }

    changeOpacity = () => {
        this.props.setTimeout(this.opacity, 500)
        this.props.setTimeout(this.opacityNone, 800)
    }


    render() {
        const user = sessionStorage.getItem('session')
        const { ButtonToggle, opacity, opacityNone  } = this.state;



        
            if(user === null || user === undefined){
                return(
                    <Route render={() => <Redirect to="/login" />}/>
                )
            }
        

        const path = this.props.location.pathname;
            return (
                <div className="App">
                    <Header changetoggle={this.ChangeToggle} togglestate={this.state.ButtonToggle} logout={this.state.Logout} />
                    <Sidebar togglestate={this.state.ButtonToggle} items={nav} path={path}/>
                    <div className="main">
                        <main className="app-content">
                            <div className={`space-8 ${ButtonToggle ? 'ml-230' : 'ml-0'}`}>
                                <Switch>
                                    <Route exact path="/" name="Home" component={Home} />
                                    <Route exact path="/config" name="Configuration" component={Config} />
                                    <Route exact path="/general" name="General" component={Others} />
                                    <Route exact path="/products" name="General" component={Products} />
                                    <Route exact path="/users" name="General" component={Users} />
                                    <Route exact path="/dashboard" name="General" component={Dashboard} />
                                    <Route render={() => <Redirect to="/404" />}/>
                                </Switch>
                            </div>
                        </main>
                    </div>
                    <div id="s1" className={`ss ${opacity ? "opacity-l" : ""}`}>
                        <div id="s2" className={`sss ${opacityNone ? "opacity-g" : ""}`}></div>
                    </div>
                </div>
            );
    }
}

export default ReactTimeout(DefaultLayout);