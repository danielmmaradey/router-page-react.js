export default 
  [
      {
        name: 'Home',
        url: '/',
        icon: 'fa fa-bar-chart',
        classNam: 'a-inicio'
      },
      {
        name: 'Config',
        url: '/config',
        icon: 'fa fa-th-large',
        classNam: 'a-config'
      },
      {
        name: 'General',
        url: '/general',
        icon: 'fa fa-cog',
        classNam: 'a-general'
      },
      {
        name: 'Products',
        url: '/products',
        icon: 'fa fa-cart-arrow-down',
        classNam: 'a-products'
      },
      {
        name: 'Users',
        url: '/users',
        icon: 'fa fa-user',
        classNam: 'a-users'
      },
      {
        name: 'Dashboard',
        url: '/dashboard',
        icon: 'fa fa-tachometer',
        classNam: 'a-dashboard'
      }
    ];

  